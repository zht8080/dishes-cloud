package com.etoak.controller;

import com.etoak.entity.Dict;
import com.etoak.service.DictService;
import common.core.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dict")
public class DictController {

    @Autowired
    DictService dictService;

    @GetMapping("/list")
    public ResultVO<List<Dict>> getList(@RequestParam String type){
        List<Dict> dictList = dictService.getList(type);
        return ResultVO.success(dictList);
    }
}
